# The task is to write a .gitlab-ci whoose pipeline does this:
- have two stages - build and run
- build stage consists of two jobs - building image for dev branch by different executors - docker dind and docker with socket forwarding
- run stage doesn't include login to registry
- run stage includes job that failes but that fail doesn't stop the whole pipeline
***

# The result
- images' building is successful and we have two images in registry
- we have a failed job and not stopped pipeline

